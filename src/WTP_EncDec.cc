///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2000-2023 Ericsson Telecom AB
//
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
///////////////////////////////////////////////////////////////////////////////
//
//  File:               WTP_EncDec.cc
//  Rev:                R1B
//  Prodnr:             CNL 113 619
//  Updated:            2008-11-13
//  Contact:            http://ttcn.ericsson.se
//  Reference:          WAP-224-WTP-20010710-a

#include "WTP_Types.hh"

namespace WTP__Types {

OCTETSTRING enc__PDU__WTP(const PDU__WTP& pdu)
{
  TTCN_Buffer bb;
  PDU__WTP pdu2(pdu);

  if (pdu2.get_selection() == PDU__WTP::ALT_concatenation)
  {  
    int record_of_size = pdu2.concatenation().wTP__PDUs().size_of();
          
    for (int  i = 0; i < (record_of_size) ; i++)
    {
       pdu2.concatenation().wTP__PDUs()[i].pDU__WTP().encode(PDU__WTP_descr_ ,bb, TTCN_EncDec::CT_RAW);
       if (bb.get_len() > 127)
       {
         pdu2.concatenation().wTP__PDUs()[i].lengthType() = int2bit(1,1);
         pdu2.concatenation().wTP__PDUs()[i].pDU__Length().long__len() = bb.get_len();               
       }
       else
       {
         pdu2.concatenation().wTP__PDUs()[i].lengthType() = int2bit(0,1);
         pdu2.concatenation().wTP__PDUs()[i].pDU__Length().short__len() = bb.get_len();         
       }
       bb.clear();                    
    }
    pdu2.encode(PDU__WTP_descr_ ,bb, TTCN_EncDec::CT_RAW);
    return OCTETSTRING (bb.get_len(), bb.get_data()); 
   }               
   else
   {
     pdu.encode(PDU__WTP_descr_ ,bb, TTCN_EncDec::CT_RAW);	  
     return OCTETSTRING (bb.get_len(), bb.get_data());                       
   }          
}

 
}
