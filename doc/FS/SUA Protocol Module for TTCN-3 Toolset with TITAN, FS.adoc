---
Author: László Farkas
Version: 155 17-CNL 113 478, Rev. A
Date: 2016-11-17

---
= SUA Protocol Modules for TTCN-3 Toolset with TITAN, Function Specification
:author: László Farkas
:revnumber: 155 17-CNL 113 478, Rev. A
:revdate: 2016-11-17
:toc:

*Copyright*

Copyright (c) 2000-2023 Ericsson Telecom AB. +
All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 that accompanies this distribution, and is available at +
http://www.eclipse.org/legal/epl-v10.html.

== How to Read This Document

This is the Function Specification for the set of SUA protocol modules. SUA protocol modules are developed for the TTCN-3 Toolset with TITAN.

== Scope

The purpose of this document is to specify the content of the SUA protocol modules.

= General

Protocol modules implement the message structures of the related protocol in a formalized way, using the standard specification language TTCN-3. This allows definition of test data (templates) in TTCN-3 language <<_2, [2]>> and correct encoding/decoding of messages when executing test suites using the Titan TTCN-3 test environment.

Protocol modules are using Titan’s RAW encoding attributes <<_3, [3]>> and hence are usable with the Titan test toolset only.

= Functional Specification

== Protocol Version Implemented

This set of protocol modules implements protocol messages and constants of the SUA protocol (see <<_1, [1]>>).

[[modifications-deviations-related-to-the-protocol-specification]]
== Modifications/Deviations Related to the Protocol Specification

=== Implemented Messages

All SUA message types of message classes 0, 2, 3, 4, 7, 8 and 9 as listed in chapter 3.1.2 of <<_1, [1]>> will be implemented.

[[protocol-modifications-deviations]]
=== Protocol Modifications/Deviations

None.

[[encoding-decoding-and-other-related-functions]]
== Encoding/Decoding and Other Related Functions

This product also contains encoding/decoding functions that assure correct encoding of messages when sent from Titan and correct decoding of messages when received by Titan. Implemented encoding/decoding functions:

[cols=3*,options=header]
|===

|Name
|Type of formal parameters
|Type of return value

|`f_enc_PDU_SUA`
|PDU_SUA
|octetstring

|`f_dec_PDU_SUA`
|octetstring
|PDU_SUA
|===

= Terminology

TITAN TTCN-3 Test Executor (see <<_3, [3]>>).

= Abbreviations

PDU:: Protocol Data Unit

SUA:: Signalling Connection Control Part User Adaptation Layer

TTCN-3:: Testing and Test Control Notation version 3

= References

[[_1]]
[1] https://tools.ietf.org/html/rfc3868 +
Signaling Connection Control Part User Adaptation Layer (SUA)

[[_2]]
[2] ETSI ES 201 873-1 v.3.1.1 (06/2005) +
The Testing and Test Control Notation version 3. Part 1: Core Language

[[_3]]
[3] User Documentation for the TITAN TTCN-3 Test Executor
